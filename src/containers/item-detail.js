import React, { Component } from 'react';
import { connect } from 'react-redux';

class ItemDetail extends Component {
    render() {
        if(!this.props.item) {
            return <div> Select a item to get started. </div>
        }

        return (
            <div>
                <h3>Details For:</h3>
                <div>Title: {this.props.item.title}</div>
                <div>Sub title: {this.props.item.subTitle}</div>
                <div>Date: {this.props.item.date}</div>
                <div>URL: {this.props.item.url}</div>
                <div>Decription: {this.props.item.description}</div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        item: state.activeItem
    };
}

export default connect(mapStateToProps)(ItemDetail);