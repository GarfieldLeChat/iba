import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SelectItem } from '../actions';
import { bindActionCreators } from 'redux';

class Itemlist extends Component {
    renderList() {
        return this.props.items.map((item) =>{
            return (
                <li 
                    className='list-group-item'
                    key={item.title} 
                    onClick={() => this.props.SelectItem(item)}>
                    {item.title}
                </li>
            );
        });
    }
    render() {
        return (
            <ul className="list-group col-sm-4">
                {this.renderList()}
            </ul>
        )
    }
}

function mapStateToProps(state) {
    return {
        items: state.items
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators( { SelectItem: SelectItem}, dispatch )
}

export default connect(mapStateToProps, mapDispatchToProps)(Itemlist);