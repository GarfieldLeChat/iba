export function SelectItem(item) {
    return {
        type: 'ITEM_SELECTED',
        payload: item
    };
}